package ru.goloshchapov.tm.exception.empty;

import ru.goloshchapov.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error! Id is empty...");
    }

}
