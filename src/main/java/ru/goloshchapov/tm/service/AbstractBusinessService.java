package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IBusinessRepository;
import ru.goloshchapov.tm.api.IBusinessService;
import ru.goloshchapov.tm.constant.SortConst;
import ru.goloshchapov.tm.enumerated.Sort;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.exception.auth.AccessDeniedException;
import ru.goloshchapov.tm.exception.empty.EmptyIdException;
import ru.goloshchapov.tm.exception.empty.EmptyNameException;
import ru.goloshchapov.tm.exception.entity.ElementsNotFoundException;
import ru.goloshchapov.tm.exception.incorrect.StatusIncorrectException;
import ru.goloshchapov.tm.exception.system.IndexIncorrectException;
import ru.goloshchapov.tm.model.AbstractBusinessEntity;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static ru.goloshchapov.tm.util.ValidationUtil.*;

public abstract class AbstractBusinessService<M extends AbstractBusinessEntity>
        extends AbstractService<M> implements IBusinessService<M> {

    @NotNull
    private final IBusinessRepository<M> businessRepository;

    @NotNull
    public AbstractBusinessService(@NotNull IBusinessRepository<M> businessRepository) {
        super(businessRepository);
        this.businessRepository = businessRepository;
    }

    private void checkDateByStatus(@NotNull final M model) {
        @NotNull final Status status = model.getStatus();
        @Nullable final Date dateStart = model.getDateStart();
        @Nullable final Date dateFinish = model.getDateFinish();
        @NotNull final Date dateNow = new Date();
        switch (status) {
            case COMPLETE:
                if (dateStart == null) model.setDateStart(dateNow);
                if (dateFinish == null) model.setDateFinish(dateNow);
                break;
            case IN_PROGRESS:
                if (dateStart == null) model.setDateStart(new Date());
                model.setDateFinish(null);
                break;
            default:
                model.setDateStart(null);
                model.setDateFinish(null);
        }
    }

    @Override
    public void addAll(@Nullable final String userId, @Nullable final Collection<M> collection) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (collection == null || collection.isEmpty()) return;
        businessRepository.addAll(userId, collection);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M add(@Nullable final String userId, @Nullable final M model) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (model == null) return null;
        return businessRepository.add(userId, model);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        @Nullable final List<M> models = businessRepository.findAll(userId);
        if (models == null) throw new ElementsNotFoundException();
        return models;
    }

    @Nullable
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator<M> comparator) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (comparator == null) return null;
        @Nullable final List<M> models = businessRepository.findAll(userId, comparator);
        if (models == null) throw new ElementsNotFoundException();
        return models;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<M> findAllByUserId(@Nullable final String userId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        @Nullable final List<M> models = businessRepository.findAllByUserId(userId);
        if (models == null) throw new ElementsNotFoundException();
        return models;
    }

    @Nullable
    @SneakyThrows
    public List<M> findAllStarted(@Nullable final String userId, @Nullable final Comparator<M> comparator) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (comparator == null) return null;
        @Nullable final List<M> models = businessRepository.findAllStarted(userId, comparator);
        if (models == null) throw new ElementsNotFoundException();
        return models;
    }

    @Nullable
    @SneakyThrows
    public List<M> findAllCompleted(@Nullable final String userId, @Nullable final Comparator<M> comparator) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (comparator == null) return null;
        @Nullable final List<M> models = businessRepository.findAllCompleted(userId, comparator);
        if (models == null) throw new ElementsNotFoundException();
        return models;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<M> sortedBy(@Nullable final String userId, @Nullable final String sortCheck) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        @Nullable final Sort[] sortOptions = Sort.values();
        @NotNull String sortChoice = SortConst.STATUS_DEFAULT;
        if (!isEmpty(sortCheck) && checkInclude(sortCheck,toStringArray(sortOptions))) sortChoice = sortCheck;
        @NotNull final Sort sortType = Sort.valueOf(sortChoice);
        switch (sortType) {
            case DATE_START: return findAllStarted(userId, sortType.getComparator());
            case DATE_FINISH: return findAllCompleted(userId, sortType.getComparator());
            default: return findAll(userId, sortType.getComparator());
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String userId, @Nullable final String modelId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(modelId)) throw new EmptyIdException();
        return businessRepository.findOneById(userId, modelId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        return businessRepository.findOneByIndex(userId, index);
    }

    @Nullable
    @SneakyThrows
    public M findOneByName(@Nullable final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        return businessRepository.findOneByName(name);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByName(@Nullable final String userId, @Nullable final String name) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        return businessRepository.findOneByName(userId, name);
    }

    public boolean isAbsentByName(@Nullable final String name) {
        if (isEmpty(name)) return true;
        return businessRepository.isAbsentByName(name);
    }

    @Nullable
    @SneakyThrows
    public String getIdByName(@Nullable final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        return businessRepository.getIdByName(name);
    }

    @Override
    @SneakyThrows
    public int size(@Nullable final String userId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        return businessRepository.size(userId);
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final String userId, @Nullable final M model) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        businessRepository.remove(userId, model);
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        businessRepository.clear(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOneById(@Nullable final String userId, @Nullable final String modelId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(modelId)) throw new EmptyIdException();
        return businessRepository.removeOneById(userId, modelId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        return businessRepository.removeOneByIndex(userId, index);
    }

    @Nullable
    @SneakyThrows
    public M removeOneByName(@Nullable final String userId, @Nullable final String name) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(name)) throw new EmptyNameException();
        return businessRepository.removeOneByName(userId, name);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M updateOneById(
            @Nullable final String userId, @Nullable final String modelId,
            @Nullable final String name, @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(modelId)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        if (businessRepository.isAbsentById(modelId)) return null;
        @Nullable final M model = findOneById(userId, modelId);
        if (model == null) return null;
        model.setId(modelId);
        model.setName(name);
        model.setDescription(description);
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M updateOneByIndex(
            @Nullable final String userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final int size = findAll().size();
        if (!checkIndex(index,size)) throw new IndexIncorrectException();
        if (isEmpty(name)) throw new EmptyNameException();
        if (businessRepository.isAbsentByIndex(index)) return null;
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        model.setName(name);
        model.setDescription(description);
        return model;
    }

    @Nullable
    @SneakyThrows
    public M startOneById(@Nullable final String userId, @Nullable final String modelId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(modelId)) throw new EmptyIdException();
        @Nullable final M model = businessRepository.startOneById(userId, modelId);
        if (model == null) return null;
        checkDateByStatus(model);
        return model;
    }

    @Nullable
    @SneakyThrows
    public M startOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final int size = businessRepository.size();
        if (!checkIndex(index,size)) throw new IndexIncorrectException();
        @Nullable final M model = businessRepository.startOneByIndex(userId, index);
        if (model == null) return null;
        checkDateByStatus(model);
        return model;
    }

    @Nullable
    @SneakyThrows
    public M startOneByName(@Nullable final String userId, @Nullable final String name) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final M model = businessRepository.startOneByName(userId, name);
        if (model == null) return null;
        checkDateByStatus(model);
        return model;
    }

    @Nullable
    @SneakyThrows
    public M finishOneById(@Nullable final String userId, @Nullable final String modelId) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(modelId)) throw new EmptyIdException();
        @Nullable final M model = businessRepository.finishOneById(userId,modelId);
        if (model == null) return null;
        checkDateByStatus(model);
        return model;
    }

    @Nullable
    @SneakyThrows
    public M finishOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final int size = businessRepository.size();
        if (!checkIndex(index,size)) throw new IndexIncorrectException();
        @Nullable final M model = businessRepository.finishOneByIndex(userId, index);
        if (model == null) return null;
        checkDateByStatus(model);
        return model;
    }

    @Nullable
    @SneakyThrows
    public M finishOneByName(@Nullable final String userId, @Nullable final String name) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final M model = businessRepository.finishOneByName(userId, name);
        if (model == null) return null;
        checkDateByStatus(model);
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M changeOneStatusById(
            @Nullable final String userId, @Nullable final String modelId, @Nullable final String statusChange
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(modelId)) throw new EmptyIdException();
        if (businessRepository.isAbsentById(modelId)) return null;
        @Nullable Status[] statuses = Status.values();
        if (!checkInclude(statusChange,toStringArray(statuses))) throw new StatusIncorrectException(statusChange);
        System.out.println(statusChange);
        @NotNull final Status status = Status.valueOf(statusChange);
        @Nullable final M model = findOneById(userId, modelId);
        if (model == null) return null;
        model.setStatus(status);
        checkDateByStatus(model);
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M changeOneStatusByName(
            @Nullable final String userId, @Nullable final String name, @Nullable final String statusChange
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(name)) throw new EmptyNameException();
        if (businessRepository.isAbsentByName(name)) return null;
        @Nullable Status[] statuses = Status.values();
        if (!checkInclude(statusChange,toStringArray(statuses))) throw new StatusIncorrectException(statusChange);
        @NotNull final Status status = Status.valueOf(statusChange);
        @Nullable final M model = findOneByName(userId, name);
        if (model == null) return null;
        model.setStatus(status);
        checkDateByStatus(model);
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M changeOneStatusByIndex(
            @Nullable final String userId, final int index, @Nullable final String statusChange) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        final int size = businessRepository.size();
        if (!checkIndex(index, size)) throw new IndexIncorrectException();
        if (businessRepository.isAbsentByIndex(index)) return null;
        @Nullable Status[] statuses = Status.values();
        if (!checkInclude(statusChange,toStringArray(statuses))) throw new StatusIncorrectException(statusChange);
        @NotNull final Status status = Status.valueOf(statusChange);
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        model.setStatus(status);
        checkDateByStatus(model);
        return model;
    }

}
