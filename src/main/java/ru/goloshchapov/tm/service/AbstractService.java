package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.IRepository;
import ru.goloshchapov.tm.api.IService;
import ru.goloshchapov.tm.exception.empty.EmptyIdException;
import ru.goloshchapov.tm.exception.entity.ElementsNotFoundException;
import ru.goloshchapov.tm.exception.entity.ProjectListIsEmptyException;
import ru.goloshchapov.tm.exception.system.IndexIncorrectException;
import ru.goloshchapov.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.List;

import static ru.goloshchapov.tm.util.ValidationUtil.*;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    private IRepository<E> repository;

    public AbstractService(@NotNull IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public void addAll(@Nullable final Collection<E> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.addAll(collection);
    }

    @Nullable
    @Override
    public E add(@Nullable final E entity) {
        if (entity == null) return null;
        return repository.add(entity);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<E> findAll() {
        @Nullable List<E> entities = repository.findAll();
        if (entities == null) throw new ElementsNotFoundException();
        return entities;
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return repository.findOneById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneByIndex(@Nullable final Integer index) {
        if (!checkIndex(index,size())) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Override
    public boolean isAbsentById(@Nullable final String id) {
        if (isEmpty(id)) return true;
        return repository.isAbsentById(id);
    }

    @Override
    public boolean isAbsentByIndex(@Nullable final Integer index) {
        if (!checkIndex(index, size())) return true;
        return repository.isAbsentByIndex(index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getIdByIndex(@Nullable final Integer index) {
        if (!checkIndex(index, size())) throw new IndexIncorrectException();
        return repository.getIdByIndex(index);
    }

    @Override
    public int size() {
        return repository.size();
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public void remove(@Nullable final E entity) {
        if (entity == null) return;
        repository.remove(entity);
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeOneById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return repository.removeOneById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeOneByIndex(@Nullable final Integer index) {
        if (!checkIndex(index,size())) throw new IndexIncorrectException();
        return repository.removeOneByIndex(index);
    }
}
