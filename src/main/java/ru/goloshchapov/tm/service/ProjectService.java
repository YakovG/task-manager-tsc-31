package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.repository.IProjectRepository;
import ru.goloshchapov.tm.api.service.IProjectService;
import ru.goloshchapov.tm.exception.auth.AccessDeniedException;
import ru.goloshchapov.tm.exception.empty.EmptyNameException;
import ru.goloshchapov.tm.model.Project;

import static ru.goloshchapov.tm.util.ValidationUtil.*;

public final class ProjectService extends AbstractBusinessService<Project> implements IProjectService {

    @NotNull private final IProjectRepository projectRepository;

    @NotNull
    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new AccessDeniedException();
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
        return project;
    }

}
